﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CompramotosFinal.Models;

namespace CompramotosFinal.Controllers
{
    public class ProductoController : Controller
    {
        private ProductoModelContainer db = new ProductoModelContainer();

        // GET: /Producto/
        public ActionResult Portafolio()
        {
            return View(db.TablaproductosSet.ToList());
        }

        public ActionResult Index()
        {
            return View(db.TablaproductosSet.ToList());
        }
        // GET: /Producto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tablaproductos tablaproductos = db.TablaproductosSet.Find(id);
            if (tablaproductos == null)
            {
                return HttpNotFound();
            }
            return View(tablaproductos);
        }

        // GET: /Producto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Producto/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Nombre,Precio,Modelo,Color,Imagenes")] Tablaproductos tablaproductos)
        {
            if (ModelState.IsValid)
            {
                db.TablaproductosSet.Add(tablaproductos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tablaproductos);
        }

        // GET: /Producto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tablaproductos tablaproductos = db.TablaproductosSet.Find(id);
            if (tablaproductos == null)
            {
                return HttpNotFound();
            }
            return View(tablaproductos);
        }

        // POST: /Producto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Nombre,Precio,Modelo,Color,Imagenes")] Tablaproductos tablaproductos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tablaproductos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tablaproductos);
        }

        // GET: /Producto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tablaproductos tablaproductos = db.TablaproductosSet.Find(id);
            if (tablaproductos == null)
            {
                return HttpNotFound();
            }
            return View(tablaproductos);
        }

        // POST: /Producto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tablaproductos tablaproductos = db.TablaproductosSet.Find(id);
            db.TablaproductosSet.Remove(tablaproductos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

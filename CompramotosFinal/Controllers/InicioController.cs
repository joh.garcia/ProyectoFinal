﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CompramotosFinal.Models;

namespace CompramotosFinal.Controllers
{
    public class InicioController : Controller
    {
        private InicioModelContainer db = new InicioModelContainer();

        // GET: /Inicio/
        public ActionResult Inicio()
        {
            return View(db.TablaInicioSet.ToList());
        }
        public ActionResult Index()
        {
            return View(db.TablaInicioSet.ToList());
        }

        // GET: /Inicio/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TablaInicio tablainicio = db.TablaInicioSet.Find(id);
            if (tablainicio == null)
            {
                return HttpNotFound();
            }
            return View(tablainicio);
        }

        // GET: /Inicio/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Inicio/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Titulo,Titulo2,Publicidad1,Publicidad2")] TablaInicio tablainicio)
        {
            if (ModelState.IsValid)
            {
                db.TablaInicioSet.Add(tablainicio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tablainicio);
        }

        // GET: /Inicio/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TablaInicio tablainicio = db.TablaInicioSet.Find(id);
            if (tablainicio == null)
            {
                return HttpNotFound();
            }
            return View(tablainicio);
        }

        // POST: /Inicio/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Titulo,Titulo2,Publicidad1,Publicidad2")] TablaInicio tablainicio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tablainicio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tablainicio);
        }

        // GET: /Inicio/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TablaInicio tablainicio = db.TablaInicioSet.Find(id);
            if (tablainicio == null)
            {
                return HttpNotFound();
            }
            return View(tablainicio);
        }

        // POST: /Inicio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TablaInicio tablainicio = db.TablaInicioSet.Find(id);
            db.TablaInicioSet.Remove(tablainicio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

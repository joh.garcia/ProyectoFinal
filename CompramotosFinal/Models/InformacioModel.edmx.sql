
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/05/2015 12:52:17
-- Generated from EDMX file: C:\Users\WILDER ALBERTO\Pictures\respaldo\ProyectoFinal\CompramotosFinal\CompramotosFinal\Models\InformacioModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Informacios];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'TablaInformacioSet'
CREATE TABLE [dbo].[TablaInformacioSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Direccion1] nvarchar(max)  NOT NULL,
    [Direccion2] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL,
    [Publicidad1] nvarchar(max)  NOT NULL,
    [Publicidad2] nvarchar(max)  NOT NULL,
    [Publicidad3] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'TablaInformacioSet'
ALTER TABLE [dbo].[TablaInformacioSet]
ADD CONSTRAINT [PK_TablaInformacioSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
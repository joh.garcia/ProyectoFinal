﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CompramotosFinal.Startup))]
namespace CompramotosFinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
